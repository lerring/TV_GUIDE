This program should be run on the console of any IDE which can run java.

Current Instructions: 

F - Forward 1 Hour
B - Backward 1 hour
S - Search for programme, returns found results, will jump to the found channel if user inputs Y.



TODO:

- Series link

- Record shows (save to a text file)
    - Save to text file
    - List recorded shows
    - Cancel recorded shows

- Move selected channel up, down, left, right
    - This will be significantly harder as the programme has no way to understand the gui
    - Make sure that it recognises what time the current channel is on, and work out the most suitable when going down
    - This is similarly coded in the monitor current show, but it only compares with the system time
    - maybe turn that into a method but input time as a parameter, works only for one show but you can loop if needed.
    - i already have a few methods in place for this which i done over summer, well done lee!
