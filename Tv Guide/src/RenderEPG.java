import java.util.Scanner;

public class RenderEPG {

	static Scanner console = new Scanner(System.in);
	static String[] timeArray = new String[25];

	static void printMenu() {

		for (int i = 0; i < 50; i++)
			System.out.println();
		
		System.out.println(Main.getDate());

		printTime();
		generateText();
		printInstructions();
		printCurrProg();


	}

	private static void printCurrProg() {
		System.out.println();
		System.out.println(Main.getCurrentShow().getShowTitle());
		System.out.println(Main.getCurrentShow().getShowDesc());
	}
	
	private static void printInstructions(){
		System.out.println("[F - FORWARD] [B - BACK] [S - SEARCH]");
	}
	
	/**
	 * Creates an array with the values 0100 to 2400, going up in increments of
	 * 100.
	 */
	private static void createTime() {
		int time = 0;
		String timeValue;
		for (int i = 0; i < timeArray.length; i++) {

			if (time == 2400) {
				time = 0;
			}
			timeValue = String.format("%04d", time);
			timeArray[i] = timeValue;
			time += 100;

		}
	}


	private static String printTime() {

		createTime();

		String timeText = ""; // Overall text

		for (int i = 0; i < Main.getDaysLoaded(); i++) {
			int ind = 0;
			for (ind = 0; ind < timeArray.length - 1; ind++) {

				String firstTime = timeArray[ind];
				String secondTime = timeArray[ind + 1];

				timeText = timeText + "|       " + firstTime + "      -       " + secondTime + "      ";

			}
		}

		String column1 = formatString("Time", 18);

		System.out.println(topRow(column1 + scroll(timeText)));
		System.out.println(spacer(column1 + scroll(timeText) + "|"));
		System.out.println(column1 + scroll(timeText) + "|");
		System.out.println(botText(column1 + scroll(timeText) + "|"));

		return timeText;
	}

	private static String spacer(String input) {

		input = input.replaceAll("[^|]", " ");
		return input;

	}

	private static String botText(String input) {
		input = input.replaceAll("[^|]", "_");

		return input;

	}

	private static String topRow(String input) {

		input = input.replaceAll(".", "_");
		input = " " + input.substring(1);
		return input;

	}

	/**
	 * This method generates the String for each channel. loops through each
	 * programme, works out how long the show is then sends the name of the
	 * programme and the length and whether it wants to be highlighted
	 * 
	 * @param channel
	 *            The current channel number
	 *
	 */
	private static void generateText() {

		//int daysLoaded = Main.getDaysLoaded();
		String text = "";

		String formattedBox;

		for (Channel channels : Main.channels) {

			text = startFiller(channels);

			for (Programme programmes : channels.programmes) {
				int showEpoch = programmes.getShowLength();
				int textLength = programmes.getTextLength(showEpoch);
			
				String showTitle = programmes.getShowTitle();
				formattedBox = formatString(showTitle, textLength);
				
			
				if (programmes.equals(Main.getCurrentShow())) {
				
				 text = text + formattedBox.replace(" ", "-");
				
				 } else {

				text = text + formattedBox;

				 }
			}
			text = padString(text);
			String altText = text.replaceAll("[a-zA-Z0-9]", " ");

			String column1 = formatString(channels.getChannelName(), 18);
			
			System.out.println(spacer(column1 + scroll(altText)) + "|");
			System.out.println(column1 + scroll(text) + "|");
			System.out.println(botText(column1 + scroll(altText)) + "|");
		}

	}

	private static String padString(String str) {

		int leng = Main.getMaxChars();
		str = str + "|";
		for (int i = str.length(); i <= leng; i++)
			str += " ";

		str = str.substring(0, leng) + "|";
		return str;
	}

	/**
	 * Makes it so the start time of the first programme fits with the time.
	 *
	 * @param channel
	 *            The current channel
	 * 
	 * @return returns a string including the start time.
	 */
	private static String startFiller(Channel channel) {

		Programme firstShow = channel.programmes.get(0);

		int channelStartEpoch = firstShow.getStartEpoch();

		int channelStartTime = firstShow.getTextLength(channelStartEpoch);

		String startTime = formatString(" ", channelStartTime);

		return startTime;
	}

	/**
	 * Formats the text to be spaced out properly
	 * 
	 * @param text
	 * @param length
	 * @return
	 */
	private static String formatString(String text, int length) {

	
		if (length < 1)
			return "|";
		else if (length < 2)
			return " |";

		// Trims a number so later the end can include "|".
		length = length - 1;

		if (text.length() != 0) {

			// If the input length is shorter than maxLength
			if (text.length() <= length) {

				for (int i = text.length(); i < length; i++) {
					if (i % 2 == 0) {
						text = " " + text;
					} else {
						text += " ";
					}
				}
			} // if the input length is larger than maxLength
			else {
				text = text.substring(0, length - 2) + "..";
			}
		}

		text = "|" + text;

		return text;

	}

	/**
	 * Takes in the string large generated for each channel and shows a
	 * selection from it, the values change when the user goes forward and
	 * backwards.
	 * 
	 * @param inputText
	 *            the string for current channel
	 * @return returns a selection of the input text to create the illusion of
	 *         scrolling.
	 */
	private static String scroll(String inputText) {

		String text = "";
		// a = where the text starts
		// b = where the text ends
		// 36 is the amount of characters an hour is
		// 4 is the amount of hours to display
	
		int a = Main.getIndex() * 36;
		int b = a + (36 * 4);

		text = inputText.substring(a, b);

		if (!text.substring(0).equalsIgnoreCase("|")) {
			return "|" + text.substring(1);
		}

		return inputText;

	}

}
