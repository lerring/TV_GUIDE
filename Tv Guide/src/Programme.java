import java.util.Scanner;

public class Programme {

	private String showTitle;
	private String showStart;
	private String showEnd;
	private String showDesc;
	private int episodeNumber;
	private String showDate;
	
	public Programme(){
	
	}

	/**
	 * Creates an object using the parameters.
	 * 
	 * @param showTitle
	 *            The title of the programme.
	 * @param showDesc
	 *            The description of the programme.
	 * @param showStart
	 *            The start time of the programme.
	 * @param showEnd
	 *            The end time of the programme.
	 */
	public Programme(String showTitle, String showDesc, String showStart, String showEnd, String showDate) {
		
		this.showTitle = showTitle;
		this.showDesc = showDesc;
		this.showStart = showStart;
		this.showEnd = showEnd;
		this.showDate = showDate;
	}

	/**
	 * Does a maths calculation to work out how many minutes from midnight the
	 * start time of a programme is.
	 * 
	 * @return Returns the minutes from midnight as an int.
	 */
	public int getStartEpoch() {
		String hoursString = this.showStart.substring(0, 2);
		String minsString = this.showStart.substring(2, 4);
		int hoursInt = Integer.parseInt(hoursString);
		int minsInt = Integer.parseInt(minsString);
		int startEpoch = (hoursInt * 60) + minsInt;
		return startEpoch;

	}
	
	/**
	 * Does a math calculation to work out how many minutes from midnight the
	 * end time of a programme is.
	 * 
	 * @return Returns the minutes from midnight as an int.
	 */
	public int getEndEpoch() {
		String hoursString = this.showEnd.substring(0, 2);
		String minsString = this.showEnd.substring(2, 4);
		int hoursInt = Integer.parseInt(hoursString);
		int minsInt = Integer.parseInt(minsString);
		int endEpoch = (hoursInt * 60) + minsInt;
		return endEpoch;
	}

	/**
	 * Gets the length of the show in minutes
	 * 
	 * @return
	 */
	public int getShowLength() {
		int start = getStartEpoch();
		int end = getEndEpoch();
		if (end < start) {
			end = end + 1440;
		}

		int lengthInMins = (end - start);
		return lengthInMins;
	}

	/**
	 * takes in the length of a show, then converts it based on 1 hour being 36 characters
	 * 
	 * @param showLength inputs the amount of minutes the show is
	 * @return
	 */
	public int getTextLength(int showLength) {
		
		showLength = showLength / 5;
		int textLength = showLength * 3;

		return textLength;
	}
			
	public String getShowTitle() {
		return showTitle;
	}

	public String getShowStart() {
		return showStart;
	}
	
	public void setShowStart(String start){
		this.showStart = start;
	}

	public String getShowEnd() {
		return showEnd;
	}

	public String getShowDesc() {
		return showDesc;
	}

	public int getEpisodeNumber() {
		return episodeNumber;
	}
	
	public String getShowDate(){
		return showDate;
	}

	@Override
	public boolean equals(Object other) {
	    
		if (!(other instanceof Programme)) {
	        return false;
	    }

	    Programme that = (Programme) other;

	    // Custom equality check here.
	    return this.showTitle.equals(that.showTitle)
	        && this.showStart.equals(that.showStart)
	        && this.showEnd.equals(that.showEnd);
	}

}
