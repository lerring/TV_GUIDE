public class Search {

	public static Programme search(String choice) {
		
		
		Programme result = searchXml(choice);
		
		if (result != null)
			return result;
		
		Programme closestMatch = findClosestMatch(choice);
			
		if (closestMatch != null)
			return closestMatch;
 
		return null;
	}

	/**
	 * @param match
	 * @return
	 */
	private static Programme findMatch(Programme match) {
		String answer;
		
		System.out.println("Show: " + match.getShowTitle()
					   + "\nDate: " + match.getShowDate()
				 + "\nStart Time: " + match.getShowStart()
				 +   "\nEnd Time: " + match.getShowEnd()
				+ "\nDescription: " + match.getShowDesc());

		System.out.println("\nIs This What You Were Looking For? [Y/N]");

		answer = Main.console.nextLine();

		do {
			if (answer.equalsIgnoreCase("Y")) {
				return match;
			} else if (!answer.equalsIgnoreCase("Y") && !answer.equalsIgnoreCase("N")) {
				System.out.println("Invalid Response, Try Again!");
				answer = Main.console.nextLine();
			}
		} while (!answer.equalsIgnoreCase("Y") && !answer.equalsIgnoreCase("N"));

		return null;
	}

	/**
	 * Does a literal search to find the result in the array.
	 * 
	 * @param  choice input as the search criteria
	 * @return returns null if there is no match, else returns a match
	 */
	private static Programme searchXml(String choice) {
		
		String programmeTitle;

		// Direct Search
		for (Channel channel : Main.channels) {

			for (Programme programme : channel.programmes) {

				programmeTitle = programme.getShowTitle();

				// If the title is the same as choice, return the title.
				if (choice.equalsIgnoreCase(programmeTitle)) {
					Programme result = findMatch(programme);

					if (result != null)
						return result;
				}
			}
		}
		
		return null;
	}
	/**
	 * @param choice
	 * @param match
	 * @param highestMatch
	 * @return 
	 */
	private static Programme findClosestMatch(String choice) {
		
		int lettersMatched;
		int highestMatch = 0;
		String programmeTitle;
		Programme match = null;
		
		// Find the closest result
		for (Channel channel : Main.channels) {
			for (Programme programme : channel.programmes) {
				lettersMatched = 0;
				programmeTitle = programme.getShowTitle();

				// for every letter in the input loop.
				lettersMatched = numberOfLettersMatched(choice, lettersMatched, programmeTitle);

				// If the Letters matched is higher than the highest current
				// match, set the match to the current programme.
				if (lettersMatched > highestMatch) {
					highestMatch = lettersMatched;
					match = programme;
				}
			}

		}

		// Print out the found match.
		if (highestMatch > 0) {

			System.out.println("Did You Mean: " + match.getShowTitle() + " [Y/N]");
			choice = Main.console.nextLine();

			if (choice.equalsIgnoreCase("Y")) {
				match = searchXml(match.getShowTitle());

				if (match != null)
				return match;
			}

		}
		
		return null;
	}

	/**
	 * @param choice
	 * @param lettersMatched
	 * @param programmeTitle
	 * @return
	 */
	private static int numberOfLettersMatched(String choice, int lettersMatched, String programmeTitle) {
		
		for (int j = 0; j < choice.length() && j < programmeTitle.length(); j++) {

			// If letter matches, letters matched goes up.
			if (programmeTitle.substring(j, j + 1).equalsIgnoreCase(choice.substring(j, j + 1))) {
				lettersMatched++;
			}
		}
		return lettersMatched;
	}

}
