import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Main {

	public static ArrayList<Channel> channels = new ArrayList<Channel>();

	private static int index;
	private static String date;
	
	final private static int daysToLoad = 4;
	final static String[] channelNames = { "bbc1", "bbc2", "ch4" };

	private static Programme currentShow;
	private static Channel currentChannel;
	static Scanner console = new Scanner(System.in);

	public static void main(String[] args) throws ParseException {
		String choice;

		initializeSystem();

		do {
			MonitorCurrentShow();
			RenderEPG.printMenu();

			choice = console.nextLine();

			switch (choice.toUpperCase()) {

			case "F": {
				increaseIndex();
				break;
			}

			case "B": {
				decreaseIndex();
				break;
			}

			case "S": {
				searchInterface();
				break;
			}
			}
		} while (!choice.equalsIgnoreCase("Q"));
		console.close();
	}

	private static void searchInterface() throws ParseException {

		System.out.println("Enter A Programme Title to Search:");
		String choice = console.nextLine();

		
		Programme searchedProgramme = Search.search(choice);

		if (searchedProgramme != null) {

			//Returns the hour of the programme you searched for
			int startHour = Integer.parseInt(searchedProgramme.getShowStart().substring(0, 2));
			System.out.println(startHour);
			console.nextLine();
			String selectedProgDate = searchedProgramme.getShowDate();
		
			// if the date is the same, go to midnight
			if (getDate().compareTo(selectedProgDate) == 0){
				
				index = index - getSystemDate("HH");
				
			} else{
				
				while(index > 0){
					decreaseIndex();
				}
	
				while (getDate().compareTo(selectedProgDate) < 0) {
					System.out.println(getDate()+  " " + selectedProgDate);
					increaseIndex();
				}
				
			}
			
			for (int i = 0; i < startHour; i++){
				increaseIndex();
			}
			
			setCurrentShow(searchedProgramme);

		} else {
			System.out.println("Could Not Find Programme, press any key");
			console.nextLine();
		}
	}

	/**
	 * This method loads all the needed data for the system to run for the first
	 * time
	 */
	public static void initializeSystem() {
		loadXml();
		loadIndex();
		loadStartingChannel();
		loadDate();
	}

	private static void loadStartingChannel() {
		Channel channel = channels.get(0);
		setCurrentChannel(channel);
	}

	/**
	 * This is used to get the current date based on the parameters needed
	 * 
	 * @return the current day
	 */
	static int getSystemDate(String format) {

		DateFormat df = new SimpleDateFormat(format);
		Date dateobj = new Date();

		return Integer.parseInt(df.format(dateobj).replaceAll("[^0-9a-zA-Z]", ""));
	}

	/**
	 * @return returns the number of days to be loaded
	 */
	public static int getDaysLoaded() {
		return daysToLoad;
	}

	/**
	 * 864 is the number of characters that makes up a day
	 * 
	 * @return works out how many characters are equal to the amount of days
	 *         that have been loaded
	 */
	public static int getMaxChars() {
		return 864 * getDaysLoaded();
	}

	/**
	 * The way the system loads in from bleb makes it so after midnight it
	 * doesnt update, this caused the programme to show the previous days
	 * programmes
	 * 
	 * 
	 * @return discrepancy is the days which it is off.
	 */
	public static int getDateDiscrepancy() {
		int currentDay = getSystemDate("dd");

		String firstLoadedDate = channels.get(0).getChannelDate();
		int loadedDateFrom = Integer.parseInt(firstLoadedDate.substring(0, 2));

		int discrepancy = currentDay - loadedDateFrom;

		return discrepancy;
	}

	/**
	 * The index is the number of hours from the first loaded programme, To make
	 * the current index be correct even when bleb hasn't updated, it compares
	 * current system date to the one loaded in, if there is a difference the
	 * index is adjusted by 24 hours
	 */
	public static void loadIndex() {

		int discrepency = getDateDiscrepancy();

		int hoursToFix = discrepency * 24;

		int index = getSystemDate("HH") + hoursToFix;

		Main.index = index;
	}

	/**
	 * Returns the index
	 * 
	 */
	public static int getIndex() {
		return index;
	}

	public static void loadDate() {
		DateFormat df = new SimpleDateFormat("dd/MM/YYY");
		Date dateobj = new Date();

		setDate(df.format(dateobj));
				
	}

	public static void setDate(String dateString) {
		Main.date = dateString;
	}
	
	public static String getDate() {
		return date;
	}

	/**
	 * increases the index, extra code is used to stop the user from crashing
	 * the system by going past the loaded shows
	 * 
	 * @throws ParseException
	 */
	public static void increaseIndex() {

		int hoursLoaded = daysToLoad * 24;

		if (index < hoursLoaded - 4) {
			index++;
		}

		if (index % 24 == 0) {

			try {
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				Calendar c = Calendar.getInstance();
				c.setTime(sdf.parse(getDate()));
				c.add(Calendar.DATE, 1); // number of days to add
				setDate(sdf.format(c.getTime()));
			} catch (ParseException e) {

			}

		}

	}

	/**
	 * if statement used to stop users from crashing the system by going too far
	 * back
	 * 
	 * @throws ParseException
	 * 
	 */
	public static void decreaseIndex() {

		if (index > 0) {
			index--;

			if (index % 24 == 0) {

				try {
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
					Calendar c = Calendar.getInstance();
					c.setTime(sdf.parse(getDate()));
					c.add(Calendar.DATE, -1); // number of days to add
					setDate(sdf.format(c.getTime()));
				} catch (ParseException e) {

				}

			}

		}

	}

	/**
	 * This method loops through all of the programmes
	 * 
	 * 
	 * 
	 */
	public static void MonitorCurrentShow() {

		Programme nearestShow = null;

		int currentTime = getSystemDate("HH:mm");
		int currentDate = getSystemDate("dd");
		int loadedDate;

		Channel channels = getCurrentChannel();

		// Loop through every programme
		for (Programme programmes : channels.programmes) {
			loadedDate = Integer.parseInt(programmes.getShowDate().substring(0, 2));

			// If the programmes date is the same as system date
			if (loadedDate == currentDate) {

				int programmeStartTime = Integer.parseInt(programmes.getShowStart());
				int programmeEndTime = Integer.parseInt(programmes.getShowEnd());

				// if the start time is before or the same as start time
				if (programmeStartTime <= currentTime && currentTime <= programmeEndTime) {
					nearestShow = programmes;
				}
			}
		}

		currentShow = nearestShow;
	}

	public static void setCurrentShow(Programme show) {
		currentShow = show;
	}

	public static Programme getCurrentShow() {
		return currentShow;
	}

	public static void setCurrentChannel(Channel channel) {
		currentChannel = channel;
	}

	public static Channel getCurrentChannel() {
		return currentChannel;
	}

	/**
	 * Loads in a specified amount of .xml files, each xml file is a channel, in
	 * every channel there is a list of programmes each programme has a title,
	 * description, start, and end time. these are read as variables and added
	 * to the programmes object.
	 */
	private static void loadXml() {
		
		Programme previousProgramme = new Programme();
		previousProgramme.setShowStart("9999");

		try {

			// Loops through each channel in the array
			for (int channelList = 0; channelList < channelNames.length; channelList++) {

				// Loops through each day
				for (int dayNumber = 0; dayNumber < daysToLoad; dayNumber++) {

					String channelNamesArray = channelNames[channelList];
					URL url = new URL("http://www.bleb.org/tv/data/listings/" + (dayNumber - 1) + "/"
							+ channelNamesArray + ".xml");
					URLConnection conn = url.openConnection();

					DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
					DocumentBuilder builder = factory.newDocumentBuilder();
					Document doc = builder.parse(conn.getInputStream());

					doc.getDocumentElement().normalize();

					// Seperates each programme out
					NodeList nList = doc.getElementsByTagName("programme");

					// When it finds a channel, gets the elements inside.
					Element channelElement = (Element) doc.getElementsByTagName("channel").item(0);

					// Gets the channel name
					String channelName = channelElement.getAttribute("id").toUpperCase();

					// Gets the channel date
					String channelDate = channelElement.getAttribute("date");

					if (dayNumber == 0) {
						Channel newChannel = new Channel(channelName, channelDate);
						channels.add(newChannel);
					}

					Channel currChannel = channels.get(channelList);

					for (int temp = 0; temp < nList.getLength(); temp++) {
						Node nNode = nList.item(temp);

						if (nNode.getNodeType() == Node.ELEMENT_NODE) {

							Element eElement = (Element) nNode;

							NodeList getTitle = eElement.getElementsByTagName("title");
							NodeList getDesc = eElement.getElementsByTagName("desc");
							NodeList getStart = eElement.getElementsByTagName("start");
							NodeList getEnd = eElement.getElementsByTagName("end");
							Element eTitle = (Element) getTitle.item(0);
							Element eDesc = (Element) getDesc.item(0);
							Element eStart = (Element) getStart.item(0);
							Element eEnd = (Element) getEnd.item(0);
							getTitle = eTitle.getChildNodes();
							getDesc = eDesc.getChildNodes();
							getStart = eStart.getChildNodes();
							getEnd = eEnd.getChildNodes();
							String aTitle = getTitle.item(0).getNodeValue();
							String aDesc = getDesc.item(0).getNodeValue();
							String aStart = getStart.item(0).getNodeValue();
							String aEnd = getEnd.item(0).getNodeValue();

							Programme addProgramme = new Programme(aTitle, aDesc, aStart, aEnd, channelDate);
							
							if(!addProgramme.equals(previousProgramme)){
								currChannel.programmes.add(addProgramme);
							}
							previousProgramme = addProgramme;

						}
					}

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
