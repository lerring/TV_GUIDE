import java.util.ArrayList;

public class Channel {
	private String channelName;
	private String channelDate;

	public ArrayList<Programme> programmes = new ArrayList<>();

	public Channel(String channelName, String date) {
		this.channelDate = date;
		this.channelName = channelName;
	}
	
	

	public String getChannelName() {
		return channelName;
	}

	public String getChannelDate() {
		return channelDate;
	}
	
	
	
	/*
	 * Gets the the text length for the first show
	 */
	public int getStartTime(){
		return programmes.get(0).getShowLength();
	}
	
	/*
	 * Gets the minutes from midnight the last show is
	 */
	public int getEndTime() {
		return programmes.get(programmes.size() - 1).getEndEpoch();
	}

}